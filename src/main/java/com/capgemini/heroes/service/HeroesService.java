package com.capgemini.heroes.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.capgemini.heroes.model.Hero;
import com.capgemini.heroes.model.Team;
import com.capgemini.heroes.repository.HeroesRepository;
import com.capgemini.heroes.repository.TeamsRepository;

/**
 * this is business layer service: it is the implementation of pattern Service (of course);
 * the Service features are the all the methods which contains calling to Repository (the DAO layer);
 *
 * https://github.com/iluwatar/java-design-patterns/tree/master/service-layer
 *
 *
 * This is a simple service, but sometimes we have to call a set of (other) services and merging final result,
 * before return it to calling: this scenario is again a Service, but it implements a specific pattern, the Facade
 *
 * https://github.com/iluwatar/java-design-patterns/tree/master/facade
 *
 *
 *
 * @author massimiliano.leone@capgemini.com
 *
 *         12 Oct 2020
 *
 */
@Service
public class HeroesService {

//	private final IHeroesRepository heroesRepository;

	private final HeroesRepository heroesRepository;
	private final TeamsRepository teamsRepository;

	@Autowired
	public HeroesService(
//			@Qualifier("heroesRepositoryI") final IHeroesRepository repository,
			@Qualifier("heroesRepository") final HeroesRepository repository,
			final TeamsRepository teamsRepository) {
		this.heroesRepository = repository;
		this.teamsRepository = teamsRepository;
	}

	public Hero addAloneHero(final String heroName, final String firstName, final String lastName) {
		Hero added = addAloneHero(HeroBuilder.build(heroName, firstName, lastName));
		return added;
	}

	public Hero addAloneHero(final Hero hero) {
		Hero saved = heroesRepository.save(hero);
		return saved;
	}

	public Hero addHeroWithinTeam(final String heroName, final String firstName, final String lastName, final String team) {
		Hero hero = HeroBuilder.build(heroName, firstName, lastName);
		Hero saved = addHeroWithinTeam(hero, team);
		return saved;
	}

	public Hero addHeroWithinTeam(final Hero hero, final String teamName) {
		Optional<Team> optionalTeam = teamsRepository.findByName(teamName);
		Team team = optionalTeam.orElse(new Team(teamName));
		if (!optionalTeam.isPresent()) {
			team = teamsRepository.save(team);
		}
		hero.setJoinedTeam(team);
		Hero savedHero = heroesRepository.save(hero);
		return savedHero;
	}

	public Iterable<Hero> retrieveAll() {
		return heroesRepository.findAll();
	}

	public Optional<Hero> retrieveById(final Long l) throws HeroesServiceException {
		if (l.longValue() == -1) {
			throw new HeroesServiceException("id is -1");
		}
		try {
			return heroesRepository.findById(l);
		} catch (IllegalArgumentException e) {
			throw new HeroesServiceException("some error occurred", e);
		} catch (NullPointerException e) {
			throw new HeroesServiceException("some error occurred", e);
		}
	}

	public Optional<Hero> retrieveByHeroName(final String string) {
		return heroesRepository.findByHeroName(string);
	}

	public List<Hero> retrieveByLastName(final String string) {
		return heroesRepository.findByLastName(string);
	}

	public static class HeroBuilder {
		public static Hero build(final long id, final String heroName, final String firstName, final String lastName) {
			Hero hero = new Hero(id, heroName, firstName, lastName);
			return hero;
		}

		public static Hero build(final String heroName, final String firstName, final String lastName) {
			Hero hero = new Hero();
			hero.setHeroName(heroName);
			hero.setFirstName(firstName);
			hero.setLastName(lastName);
			return hero;
		}
	}

	public static class HeroesServiceException extends Exception {

		private static final long serialVersionUID = 5758171794316701803L;

		public HeroesServiceException() {
			super();
		}

		public HeroesServiceException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
			super(message, cause, enableSuppression, writableStackTrace);
		}

		public HeroesServiceException(final String message, final Throwable cause) {
			super(message, cause);
		}

		public HeroesServiceException(final String message) {
			super(message);
		}

		public HeroesServiceException(final Throwable cause) {
			super(cause);
		}
	}

}
