/**
 *
 */
package com.capgemini.heroes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.capgemini.heroes.model.Hero;

/**
 * @author massimiliano.leone@capgemini.com
 *
 *         27 Oct 2020
 *
 */
public class HeroesRESTClient {

	private static final Logger log = LoggerFactory.getLogger(HeroesRESTClient.class);

	public List<Hero> searchHeroesRemotely() {
		RestTemplate restTemplate = new RestTemplate();
		String url = "http://localhost:8080/ws/rest/controller/heroes";
		ResponseEntity<Hero[]> response = restTemplate.getForEntity(url, Hero[].class);
		log.info("status: " + response.getStatusCode());
		Hero[] body = response.getBody();
		return Arrays.asList(body);
	}

	public List<Hero> searchHeroesRemotelyUsingWrapper() {
		RestTemplate restTemplate = new RestTemplate();
		String url = "http://localhost:8080/ws/rest/controller/heroes";
		ResponseWrapper response = restTemplate.getForObject(url, ResponseWrapper.class);
		List<Hero> heroes = response.get_embedded().getHeroes();
		return heroes;
	}

	public static class ResponseWrapper {
		private Embedded _embedded;

		public Embedded get_embedded() {
			return _embedded;
		}

		public void set_embedded(final Embedded _embedded) {
			this._embedded = _embedded;
		}
	}

	public static class Embedded {
		private List<Hero> heroes;

		public Embedded() {
			this.heroes = new ArrayList<>();
		}

		public List<Hero> getHeroes() {
			return heroes;
		}

		public void setHeroes(final List<Hero> heroes) {
			this.heroes = heroes;
		}
	}

}
