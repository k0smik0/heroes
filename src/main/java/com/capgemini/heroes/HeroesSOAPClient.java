/**
 *
 */
package com.capgemini.heroes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import com.capgemini.heroes.ws.soap.model.HeroDTODetailsRequest;
import com.capgemini.heroes.ws.soap.model.HeroDTODetailsResponse;

/**
 * @author massimiliano.leone@capgemini.com
 *
 *         10 Nov 2020
 *
 */
public class HeroesSOAPClient extends WebServiceGatewaySupport {

	private static final Logger log = LoggerFactory.getLogger(HeroesSOAPClient.class);

	public HeroDTODetailsResponse getHero(final String hero) {

		HeroDTODetailsRequest request = new HeroDTODetailsRequest();
		request.setHeroName(hero);

		log.info("Requesting hero details for " + hero);

		HeroDTODetailsResponse response = (HeroDTODetailsResponse) getWebServiceTemplate()
			.marshalSendAndReceive("http://localhost:8080/ws/soap/heroes", request,
					new SoapActionCallback("http://spring.io/guides/gs-producing-web-service/GetCountryRequest"));

		return response;
	}

}
