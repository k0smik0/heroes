package com.capgemini.heroes;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.shell.SpringShellAutoConfiguration;

import com.capgemini.heroes.model.Hero;
import com.capgemini.heroes.service.HeroesService;
import com.capgemini.heroes.service.HeroesService.HeroBuilder;
import com.capgemini.heroes.service.HeroesService.HeroesServiceException;
import com.github.fonimus.ssh.shell.SshShellAutoConfiguration;

/**
 *
 * @author massimiliano.leone@capgemini.com
 *
 *         12 Oct 2020
 *
 */
// this enable any SpringBoot feature
@SpringBootApplication
// this import a Shell + SSHShell configuration in order to have a ssh connection to our application
// try connecting with: ssh -p 2222 user@localhost [password and other details are into applicaiton-default.properties]
@ImportAutoConfiguration(classes = { SshShellAutoConfiguration.class, SpringShellAutoConfiguration.class })
public class HeroesApplication {

	private final String b = "pippo";

	private static final Logger log = LoggerFactory.getLogger(HeroesApplication.class);

	public static void main(final String[] args) {
		SpringApplication.run(HeroesApplication.class, args);

	}

	// CommandLineRunner is an executor to run some code just after boot
	// we could run that code into main, just after "SpringApplication.run",
	// but we may not profit of Spring injection for any object we would use.
	// Instead, into CommandLineRunner we could use any injected class passing as parameter
	@Bean
	public CommandLineRunner demo(final HeroesService service) {

		/*-
		// anonymous inner class
		CommandLineRunner c1 = new CommandLineRunner() {
			@Override
			public void run(final String... _args) throws Exception {
				addHeroes(service);
				showHeroes(service);
			}
		};
		
		// lambda
		CommandLineRunner c2 = _args -> {
			addHeroes(service);
			showHeroes(service);
		};
		*/

		// better using lambda+closure

		// String[] args are exactly the "String[] args" from main,
		// so we could process here command line arguments
		return (_args) -> {
			addHeroes(service);
			showHeroes(service);
		};
	}

	// inserting some heroes, real or from some fantasy novel
	// who guess the real hero ;D ?
	private void addHeroes(final HeroesService service) {
		// add a few heroes

		// passing hero,team
		service.addHeroWithinTeam(HeroBuilder.build("Batman", "Bruce", "Wayne"), "Justice Of League");
		// passing all "exploded" parameters
		service.addHeroWithinTeam("Colonel", "Hannibal", "Smith", "A-Team");

		service.addHeroWithinTeam(HeroBuilder.build("IronMan", "Tony", "Stark"), "Avengers");
		service.addHeroWithinTeam(HeroBuilder.build("Wolverine", "John", "Logan"), "X-Men");

		// they do not join any team
		service.addAloneHero(HeroBuilder.build("Paperinik", "Paolino", "Paperino"));
		service.addAloneHero(HeroBuilder.build("MrDecrypt", "Alan", "Turing"));

		service.addHeroWithinTeam(HeroBuilder.build("Superman", "Clark", "Kent"), "Justice Of League");
		service.addHeroWithinTeam(HeroBuilder.build("Flash", "Barry", "Allen"), "Justice Of League");
		service.addHeroWithinTeam(HeroBuilder.build("Spiderman", "Peter", "Parker"), "Avengers");

		/*-
		 who is? Damon Wayne is the son of Batman, and he's the third Robin after:
		  - Dick Grayson (the first, well known, Robin - actually Nightwing)
		  - Jason Todd
		  - Tim Drake (became Red Robin)
		 */
		service.addAloneHero(HeroBuilder.build("Robin (Third)", "Damian", "Wayne"));
	}

	private void showHeroes(final HeroesService service) {
		// fetch all heroes
		log.info("Heroes found with findAll():");
		log.info("-------------------------------");
		for (Hero hero : service.retrieveAll()) {
			log.info(hero.toString());
		}
		log.info("");

		// fetch an individual hero by ID
		try {
			Optional<Hero> hero = service.retrieveById(1L);
			hero.ifPresent(h -> {
				log.info("Hero found with findById(1L):");
				log.info("--------------------------------");
				log.info(h.toString());
				log.info("");
			});
		} catch (HeroesServiceException e) {
			log.error("got an exception", e);
		}

		// fetch heroes by last name
		log.info("Customer found with retrieveByLastName('Wayne'):");
		log.info("--------------------------------------------");

		List<Hero> retrievedByLastName = service.retrieveByLastName("Wayne");
		retrievedByLastName
			.forEach(p -> {
				log.info(p.toString());
			});
		// is an alias to:
		/*-
		retrievedByLastName.stream().forEach(p -> {
			log.info(p.toString());
		});
		*/

		/*-
		// using parallelism
		// old way
		ExecutorService e = Executors.newWorkStealingPool();
		for (Hero hero : retrievedByLastName) {
			Runnable r = () -> log.info(hero.toString());
			e.execute(r);
		}
		
		// or new stream+lambda way
		retrievedByLastName
		.stream().parallel()
		.forEach(p -> {
			log.info(p.toString());
		});
		*/

//		String output = "";
//		for (Hero hero : service.retrieveAll()) {
//			output+=" ; "+hero.toString();
//		}
//		// a ; b ; c ; d ;
//		// ; a ; b ; c ; d
//		output.replaceFirst(" ; ","");
		// a ; b ; c ; d

		// or if we want a concatenated string as result:
		// String collected = retrievedByLastName.stream().map(h -> h.toString()).collect(Collectors.joining(" ; "));
		// log.info(collected);
		// it prints the following:
		// "Bruce Wayne Batman; Damon Wayne Robin"

		log.info("");

	}

}
