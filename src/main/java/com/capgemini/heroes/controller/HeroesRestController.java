/**
 *
 */
package com.capgemini.heroes.controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.heroes.model.Hero;
import com.capgemini.heroes.service.HeroesService;
import com.capgemini.heroes.service.HeroesService.HeroesServiceException;

/**
 * @author massimiliano.leone@capgemini.com
 *
 *         26 Oct 2020
 *
 */
@RestController
@RequestMapping(value = "/ws/rest/controller", produces = "application/hal+json")
public class HeroesRestController {

	private static final Logger log = LoggerFactory.getLogger(HeroesRestController.class);

	private final HeroesService heroesService;

	@Autowired
	public HeroesRestController(final HeroesService heroesService) {
		this.heroesService = heroesService;
		log.info("instanced");
	}

	@GetMapping("/hero/search")
	public HttpEntity<EntityModel<Hero>> findHero(@RequestParam(value = "heroName"/* , defaultValue = "Batman" */) final String heroName) {
		Optional<Hero> retrievedByHeroName = heroesService.retrieveByHeroName(heroName);
		ResponseEntity<EntityModel<Hero>> response = retrievedByHeroName
			.map(h -> {
				Link link = WebMvcLinkBuilder.linkTo(HeroesRestController.class).slash("/hero").slash(h.getId()).withSelfRel();
				return new ResponseEntity<>(EntityModel.of(h, link), HttpStatus.OK);
			})
			.orElse(new ResponseEntity<>(HttpStatus.NO_CONTENT));
		return response;
	}

	@GetMapping("/hero/{heroId}")
	public HttpEntity<EntityModel<Hero>> getHero(@PathVariable(value = "heroId") final Long heroId) {
		if (heroId == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		try {
			Optional<Hero> retrievedByHeroName = heroesService.retrieveById(heroId);
			ResponseEntity<EntityModel<Hero>> response = retrievedByHeroName
				.map(h -> {
					EntityModel.of(h);
					// Link link = WebMvcLinkBuilder.linkTo(HeroesRestController.class).slash("/hero").slash(h.getId()).withSelfRel();
					// or use the method who autoretrieve the path from controller method
					Link link = buildLink(h);
					return new ResponseEntity<>(EntityModel.of(h, link), HttpStatus.OK);
				})
				.orElse(new ResponseEntity<>(HttpStatus.NO_CONTENT));
			return response;
		} catch (HeroesServiceException e) {
			log.error("error retrieving " + heroId, e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/heroes")
	public ResponseEntity<CollectionModel<EntityModel<Hero>>> findHeroes() {
		Iterable<Hero> retrievedAll = heroesService.retrieveAll();
		List<EntityModel<Hero>> heroes = StreamSupport.stream(retrievedAll.spliterator(), false)
			.map(hero -> EntityModel.of(hero, buildLink(hero), buildLinks()))
			.collect(Collectors.toList());
		if (heroes.size() == 0) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return ResponseEntity.ok(CollectionModel.of(heroes, buildLinks()));
	}

	private Link buildLink(final Hero hero) {
		Link withSelfRel = WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(HeroesRestController.class).getHero(hero.getId())).withSelfRel();
		return withSelfRel;
	}

	private Link buildLinks() {
		Link withRel = WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(HeroesRestController.class).findHeroes()).withRel("heroes");
		return withRel;
	}

	/*-
	private ResponseEntity buildResponseOK(final Hero h) {
		EntityModel.of(h);
		Link link = WebMvcLinkBuilder.linkTo(HeroesRestController.class).slash(h.getId()).withSelfRel();
		return new ResponseEntity<>(EntityModel.of(h, link), HttpStatus.OK);
	}
	*/

}
