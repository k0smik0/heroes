/**
 *
 */
package com.capgemini.heroes.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellCommandGroup;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

import com.capgemini.heroes.HeroesRESTClient;
import com.capgemini.heroes.HeroesSOAPClient;
import com.capgemini.heroes.model.Hero;
import com.capgemini.heroes.service.HeroesService;
import com.capgemini.heroes.ws.soap.model.HeroDTO;
import com.capgemini.heroes.ws.soap.model.HeroDTODetailsResponse;
import com.github.fonimus.ssh.shell.commands.SshShellComponent;

/**
 * @author massimiliano.leone@capgemini.com
 *
 *         12 Oct 2020
 *
 */
@SshShellComponent
@ShellComponent
@ShellCommandGroup
public class HeroesShell {

	private static final Logger log = LoggerFactory.getLogger(HeroesShell.class);

	@Autowired
	private HeroesService heroService;

	@Autowired
	private HeroesSOAPClient heroesSOAPClient;

//	@Autowired
//	private ActuatorCommand actuator;
//	@Autowired
//	private SshShellHelper helper;

	@ShellMethod("just regards")
	public String regards() {
		return "hello boyz, hello girlz, here we go!";
	}

	@ShellMethod("search your hero!")
	public String searchYourHero(final String heroName) {
		String found = heroService.retrieveByHeroName(heroName).map(h -> h.toString()).orElse("sorry, no hero found");
		log.info("found: " + found);
		return found;
	}

	/**
	 * a little sample to demonstrate the rest client calling using REST Template, but getting an error
	 * @param  heroName
	 * @return
	 */
	@ShellMethod("search your heroes on remote!")
	public String searchHeroesRemotelyUsingREST() {
		List<Hero> searchHeroesRemotely = new HeroesRESTClient().searchHeroesRemotely();
		String collect = searchHeroesRemotely.stream().map(h -> h.toString()).collect(Collectors.joining("\n"));
		String error = "this method got an error for learning purpose! try instead 'searchHeroesRemotelyUsingWrapper'";
		return collect + "\n\n" + error;
	}

	/**
	 * a little sample to demonstrate the rest client calling using REST Template
	 * @return
	 */
	@ShellMethod("search your heroes [wrapper way]!")
	public String searchHeroesRemotelyUsingRESTWithWrapper() {
		List<Hero> searchHeroesRemotely = new HeroesRESTClient().searchHeroesRemotelyUsingWrapper();
		String collect = searchHeroesRemotely.stream().map(h -> h.toString()).collect(Collectors.joining("\n"));
		return collect;
	}

	/**
	 * a little sample to demonstrate the rest client calling using WebService Template
	 * @return
	 */
	@ShellMethod("search your hero [soap way]!")
	public String searchHeroRemotelyUsingSOAP(final String heroName) {
		HeroDTODetailsResponse heroResponse = heroesSOAPClient.getHero(heroName);

		HeroDTO heroDTO = heroResponse.getHeroDTO();
		if (heroDTO != null) {
			Hero hero = new Hero(-10, heroDTO.getHeroName(), heroDTO.getFirstName(), heroDTO.getLastName());
			return hero.toString();
		}

		return "sorry, not found";
	}

	/*-
	private static final int MAX_HALT_WAITING = 7 * 1000; // sec
	
	@ShellMethod("shutdown application, instantly")
	public String halt() {
		log.info("shutdown started...");
		// Main.STATUS.EXITING.setExiting();
		String shutdownResult = actuator.shutdown();
		buildHaltThread().run();
		return shutdownResult;
	}
	
	private static Thread buildHaltThread() {
		Thread thread = new Thread() {
			@Override
			public void run() {
				try {
					Thread.sleep(MAX_HALT_WAITING);
				} catch (InterruptedException e) {
				}
				log.info("shutdown waiting too long, force exit");
				System.exit(-1);
			}
		};
		return thread;
	}
	*/

}
