/**
 *
 */
package com.capgemini.heroes.repository;

import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.capgemini.heroes.model.Team;

/**
 * Repository implements the DAO pattern: we have an interface with some method
 * and let the framework (JPA) implements the method in a way honoring the subexistant db.
 * In this sample we are using H2, a in-memory db, but simply changing some configuration
 * we could switch to a real db (Oracle, PostGreSQL, Microsoft SQLServer, whatever)
 *
 * Of course, if we need some specific query using native code, we could use an abstract class,
 * where we will implement by ourselves those methods, and let the other ones abstract,
 * handled (=implemented) by JPA
 *
 * https://github.com/iluwatar/java-design-patterns/tree/master/dao
 *
 *
 * @author massimiliano.leone@capgemini.com
 *
 *         14 Oct 2020
 *
 */
@RepositoryRestResource(collectionResourceRel = TeamsRepository.REPOSITORY_RESOURCE, path = TeamsRepository.REPOSITORY_RESOURCE)
public interface TeamsRepository extends PagingAndSortingRepository<Team, Long> {

	String REPOSITORY_RESOURCE = "teams";

	Optional<Team> findByName(String name);

//	Optional<Team> findByJoiningHero(Hero joiningHero);

}
