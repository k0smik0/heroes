/**
 *
 */
package com.capgemini.heroes.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.capgemini.heroes.model.Hero;

/**
 * @author massimiliano.leone@capgemini.com
 *
 *         26 Oct 2020
 *
 */
@RepositoryRestResource(collectionResourceRel = HeroesRepository.REPOSITORY_RESOURCE, path = HeroesRepository.REPOSITORY_RESOURCE)
public interface HeroesRepository extends PagingAndSortingRepository<Hero, Long> {

	String REPOSITORY_RESOURCE = "heroes";

	List<Hero> findByLastName(@Param("lastName") String lastName);

	Optional<Hero> findByHeroName(@Param("heroName") String heroName);

}
