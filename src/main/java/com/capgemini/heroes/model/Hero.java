package com.capgemini.heroes.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author massimiliano.leone@capgemini.com
 *
 *         12 Oct 2020
 *
 */
@Table(name = "heroes")
//@Relation(collectionRelation = "heroes", itemRelation = "hero")
@Entity
public class Hero {

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
//	@JsonIgnore
	private long id;

	private String heroName;
	private String firstName;
	private String lastName;

	@ManyToOne
	@JoinColumn(name = "hero_id_to_team_id")
	private Team joinedTeam;

	public Hero() {}

	public Hero(final long id, final String heroName, final String firstName, final String lastName) {
		this.id = id;
		this.heroName = heroName;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public long getId() {
		return id;
	}

	public void setId(final long id) {
		this.id = id;
	}

	public String getHeroName() {
		return heroName;
	}

	public void setHeroName(final String heroName) {
		this.heroName = heroName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(final String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(final String lastName) {
		this.lastName = lastName;
	}

	public void setJoinedTeam(final Team joinedTeam) {
		this.joinedTeam = joinedTeam;
	}

	public Team getJoinedTeam() {
		return joinedTeam;
	}

	@Override
	public String toString() {
		return id + ":: " + heroName + ": " + firstName + " " + lastName
				+ (joinedTeam != null ? ", joinedTeam: " + joinedTeam.getName() : "");
	}
}
