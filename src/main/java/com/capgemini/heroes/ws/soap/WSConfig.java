package com.capgemini.heroes.ws.soap;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

import com.capgemini.heroes.HeroesSOAPClient;

/**
 * @author massimiliano.leone@capgemini.com
 *
 *         12 Oct 2020
 *
 */
@EnableWs
@Configuration
public class WSConfig extends WsConfigurerAdapter {

//	static final String NAMESPACE_URI = "http://capgemini.com/heroes/ws/soap/model";

	/* EXPOSE ZONE */

	private static final String ROOT = "/ws/soap";

	@Bean
	public ServletRegistrationBean messageDispatcherServlet(final ApplicationContext applicationContext) {
		MessageDispatcherServlet servlet = new MessageDispatcherServlet();
		servlet.setApplicationContext(applicationContext);
		servlet.setTransformWsdlLocations(true);
		return new ServletRegistrationBean(servlet, ROOT + "/*");
	}

	@Bean(name = "heroDTODetailsWsdl")
	public DefaultWsdl11Definition defaultWsdl11Definition(final XsdSchema schema) {
		DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
		wsdl11Definition.setPortTypeName("HeroDTODetailsPort");
		wsdl11Definition.setLocationUri(ROOT + "/service/hero");
		wsdl11Definition.setTargetNamespace(HeroesWS.NAMESPACE_URI);
		wsdl11Definition.setSchema(schema);
		return wsdl11Definition;
	}

	@Bean
	public XsdSchema heroSchema() {
		return new SimpleXsdSchema(new ClassPathResource("hero.xsd"));
	}

	/* CONSUMING ZONE */

	@Bean
	public Jaxb2Marshaller marshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		// this package must match the package in the <generatePackage> specified in pom.xml
		marshaller.setContextPath("com.capgemini.heroes.ws.soap.model");
		return marshaller;
	}

	@Bean
	public HeroesSOAPClient heroesSOAPClient(final Jaxb2Marshaller marshaller) {
		HeroesSOAPClient client = new HeroesSOAPClient();
		client.setDefaultUri("http://localhost:8080/ws/soap");
		client.setMarshaller(marshaller);
		client.setUnmarshaller(marshaller);
		return client;
	}
}
