package com.capgemini.heroes.ws.soap;

import java.util.Optional;

import javax.jws.WebService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.capgemini.heroes.model.Hero;
import com.capgemini.heroes.service.HeroesService;
import com.capgemini.heroes.ws.soap.model.HeroDTO;
import com.capgemini.heroes.ws.soap.model.HeroDTODetailsRequest;
import com.capgemini.heroes.ws.soap.model.HeroDTODetailsResponse;

/**
 * @author massimiliano.leone@capgemini.com
 *
 *         12 Oct 2020
 *
 */
@Endpoint
@WebService(serviceName = "hero")
public class HeroesWS {

	static final String NAMESPACE_URI = "http://capgemini.com/heroes/ws/soap/model";

	@Autowired
	private HeroesService heroesService;

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "HeroDTODetailsRequest")
	@ResponsePayload
	public HeroDTODetailsResponse retrieveByHeroName(@RequestPayload final HeroDTODetailsRequest request) {
		HeroDTODetailsResponse heroDTODetailsResponse = new HeroDTODetailsResponse();

		Optional<Hero> retrievedByHeroName = heroesService.retrieveByHeroName(request.getHeroName());
		retrievedByHeroName.ifPresent(hero -> {
			HeroDTO heroDTO = new HeroDTO();
			heroDTO.setFirstName(hero.getFirstName());
			heroDTO.setLastName(hero.getLastName());
			heroDTO.setHeroName(hero.getHeroName());
			heroDTODetailsResponse.setHeroDTO(heroDTO);
		});

		return heroDTODetailsResponse;
	}

}
