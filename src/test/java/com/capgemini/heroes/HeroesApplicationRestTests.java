/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.capgemini.heroes;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.capgemini.heroes.repository.HeroesRepository;

@SpringBootTest
@AutoConfigureMockMvc
public class HeroesApplicationRestTests {

	private static final String ROOT = "/ws/rest/";
	private static final String RESOURCE = HeroesRepository.REPOSITORY_RESOURCE;

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	@Qualifier("heroesRepository")
	private HeroesRepository personRepository;

	@BeforeEach
	public void deleteAllBeforeTests() throws Exception {
		personRepository.deleteAll();
	}

	@Test
	public void shouldReturnRepositoryIndex() throws Exception {

		mockMvc.perform(
				get(ROOT))
			.andDo(print())
			.andExpect(status().isOk())
			.andExpect(jsonPath("$._links." + RESOURCE).exists());
	}

	@Test
	public void shouldCreateEntity() throws Exception {
		MockHttpServletRequestBuilder post = post(ROOT + RESOURCE).content("{\"heroName\": \"MrDecrypt\", \"firstName\": \"Alan\", \"lastName\":\"Turing\"}");
		ResultActions perform = mockMvc.perform(post);
		perform.andExpect(status().isCreated())
			.andExpect(header().string("Location", containsString(RESOURCE + "/")));
	}

	@Test
	public void shouldRetrieveEntity() throws Exception {

		MvcResult mvcResult = mockMvc.perform(post(ROOT + RESOURCE).content("{\"heroName\": \"MrDecrypt\", \"firstName\": \"Alan\", \"lastName\":\"Turing\"}"))
			.andExpect(status().isCreated())
			.andReturn();

		String location = mvcResult.getResponse().getHeader("Location");
		mockMvc.perform(get(location)).andExpect(status().isOk()).andExpect(jsonPath("$.firstName").value("Alan"))
			.andExpect(jsonPath("$.lastName").value("Turing"));
	}

	@Test
	public void shouldQueryEntity() throws Exception {

		mockMvc.perform(post(ROOT + RESOURCE).content("{ \"heroName\": \"MrDecrypt\", \"firstName\": \"Alan\", \"lastName\":\"Turing\"}"))
			.andExpect(status().isCreated());

		mockMvc.perform(get(ROOT + RESOURCE + "/search/findByLastName?lastName={lastName}", "Turing"))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$._embedded." + RESOURCE + "[0].lastName").value("Turing"));
	}

	@Test
	public void shouldUpdateEntity() throws Exception {

		MvcResult mvcResult = mockMvc.perform(post(ROOT + RESOURCE).content("{\"heroName\": \"MrDecrypt\", \"firstName\": \"Alan\", \"lastName\":\"Turing\"}"))
			.andExpect(status().isCreated())
			.andReturn();

		String location = mvcResult.getResponse().getHeader("Location");

		mockMvc.perform(put(location).content("{\"heroName\": \"MrDecrypt\", \"firstName\": \"Alan\", \"lastName\":\"Turing\"}"))
			.andExpect(status().isNoContent());

		mockMvc.perform(get(location))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.firstName").value("Alan"))
			.andExpect(jsonPath("$.lastName").value("Turing"));
	}

	@Test
	public void shouldPartiallyUpdateEntity() throws Exception {

		MvcResult mvcResult = mockMvc.perform(post(ROOT + RESOURCE).content("{\"heroName\": \"MrDecrypt\", \"firstName\": \"Alan\", \"lastName\":\"Turing\"}"))
			.andExpect(status().isCreated())
			.andReturn();

		String location = mvcResult.getResponse().getHeader("Location");

		mockMvc.perform(patch(location).content("{\"firstName\": \"Alan Jr.\"}"))
			.andExpect(status().isNoContent());

		mockMvc.perform(get(location)).andExpect(status().isOk())
			.andExpect(jsonPath("$.firstName").value("Alan Jr."))
			.andExpect(jsonPath("$.lastName").value("Turing"));
	}

	@Test
	public void shouldDeleteEntity() throws Exception {

		MvcResult mvcResult = mockMvc.perform(post(ROOT + RESOURCE).content("{ \"heroName\": \"MrDecrypt\", \"firstName\": \"Alan\", \"lastName\":\"Turing\"}"))
			.andExpect(status().isCreated())
			.andReturn();

		String location = mvcResult.getResponse().getHeader("Location");
		mockMvc.perform(delete(location)).andExpect(status().isNoContent());

		mockMvc.perform(get(location)).andExpect(status().isNotFound());
	}
}
